from aoc import read_input, Oceanfloor

inputfile = 'input1.txt'
my_input = read_input(inputfile)
my_input = [int(l) for l in my_input]
my_oceanfloor = Oceanfloor(my_input)


print(my_oceanfloor.amount_deeper())
print(my_oceanfloor.amount_deeper_noisy())

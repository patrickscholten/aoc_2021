from aoc import read_input, Fish

# inputfile = 'input6-test.txt'
inputfile = 'input6.txt'
my_input = [int(x) for x in read_input(inputfile)[0].split(',')]
d = dict()
for x in range(9):
    d[x] = my_input.count(x)

f = Fish(d)
# days = 80
days = 256
for d in range(days):
    f.next_day()
print(f.amount_fish())

from aoc import read_input, Submarine


my_sub = Submarine(0, 0, 0)
my_input = read_input('input2.txt')
for m in my_input:
    direction = m.split()[0]
    amount = int(m.split()[1])
    my_sub.move(direction, amount)
print(my_sub.current_position())
print(my_sub.multiply())

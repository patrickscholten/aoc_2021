def read_input(filename):
    with open(filename) as fh:
        lines = fh.read().splitlines()
    return lines

def most_common_bit(my_list: list, pos: int):
    number_of_zeros = 0
    number_of_ones = 0
    for n in my_list:
        if n[pos] == '0':
            number_of_zeros += 1
        elif n[pos] == '1':
            number_of_ones += 1
        else:
            return False
    if number_of_zeros > number_of_ones:
        return '0'
    elif number_of_ones > number_of_zeros:
        return '1'
    else:
        return 'equal'


def least_common_bit(my_list: list, pos: int):
    most_common = most_common_bit(my_list, pos)
    if most_common == '0':
        return '1'
    elif most_common == '1':
        return '0'
    else:
        return 'equal'


class Oceanfloor:
    def __init__(self, depth):
        self.depth = depth
    
    def amount_deeper(self):
        deeper = 0
        for n in range(len(self.depth) - 1):
            if (self.depth[n] < (self.depth[n+1])):
                deeper += 1
        return deeper
    
    def amount_deeper_noisy(self):
        deeper = 0
        for x in range(len(self.depth)-3):
            window_a = self.depth[x] + self.depth[x+1] + self.depth[x+2]
            window_b = self.depth[x+1] + self.depth[x+2] + self.depth[x+3]
            if window_a < window_b:
                deeper += 1
        return deeper


class Submarine:
    def __init__(self, horizontal_pos: int, depth: int, aim: int):
        self.horizontal_pos = horizontal_pos
        self.depth = depth
        self.aim = aim

    def move(self, direction: str, amount: int):
        if direction == 'forward':
            self.forward(amount)
        elif direction == 'down':
            self.down(amount)
        elif direction == 'up':
            self.up(amount)
        else:
            return False
        
    def forward(self, amount: int):
        self.horizontal_pos += amount
        self.depth += (self.aim * amount)

    def down(self, amount: int):
        self.aim += amount

    def up(self, amount: int):
        self.aim -= amount

    def current_position(self):
        return "Horizontal position: ", self.horizontal_pos, "Depth: ", self.depth
    
    def multiply(self):
        return self.horizontal_pos * self.depth


class Report:
    def __init__(self, report):
        self.report = report

    def gamma_rate(self):
        # Take first item of report to determine the length one item
        gamma = ''
        for n in range(len(self.report[0])):
            most_common = most_common_bit(self.report, n)
            gamma += most_common
        return gamma


    def epsilon_rate(self):
        gamma = self.gamma_rate()
        epsilon = ''
        for n in gamma:
            if n == '0':
                epsilon += '1'
            else:
                epsilon += '0'
        return epsilon


    def power_consumption(self):
        gamma = int(self.gamma_rate(), 2)
        epsilon = int(self.epsilon_rate(), 2)
        return gamma * epsilon


    def oxygen_generator_rating(self):
        pos = 0
        oxy_list = self.report
        while len(oxy_list) > 1:
            keep = []
            most_common = most_common_bit(oxy_list, pos)
            if most_common == 'equal':
                most_common = '1'
            for i in range(len(oxy_list)):
                if oxy_list[i][pos] == most_common:
                    keep.append(oxy_list[i])
            pos += 1
            oxy_list = keep
        return oxy_list[0]


    def co2_scrubber_rating(self):
        pos = 0
        co2_list = self.report
        while len(co2_list) > 1:
            keep = []
            least_common = least_common_bit(co2_list, pos)
            if least_common == 'equal':
                least_common = '0'
            for i in range(len(co2_list)):
                if co2_list[i][pos] == least_common:
                    keep.append(co2_list[i])
            pos += 1
            co2_list = keep
        return co2_list[0]

    def life_support_rating(self):
        oxy = int(self.oxygen_generator_rating(), 2)
        co2 = int(self.co2_scrubber_rating(), 2)
        return oxy * co2


class Bingoboard:
    def __init__(self, board: list) -> None:
        # A board is a list of 5 items. Each items is a string with 5 numbers.
        self.board = board
        self.rows = self.rows()
        self.columns = self.columns()

    def rows(self):
        rows = []
        for i in self.board:
            row = i.split()
            rows.append(row)
        return rows

    def columns(self):
        columns = []
        for i in range(5):
            column = []
            for j in range(5):
                column.append(self.rows[j][i])
            columns.append(column)
        return columns

    def is_winner(self):
        for r in self.rows:
            if len(r) == 0:
                return True
        for c in self.columns:
            if len(c) == 0:
                return True
        return False

    def draw(self, number):
        for c in self.columns:
            try:
                c.remove(number)
            except ValueError:
                pass
        for r in self.rows:
            try:
                r.remove(number)
            except ValueError:
                pass

    def remaining_sum(self):
        sum = 0
        for r in self.rows:
            for x in r:
                sum += int(x)
        return sum


class Vent:
    def __init__(self, line) -> None:
        #  A line is 'startx,starty -> endx,endy'
        # for example '0,9 -> 5,9'
        # lines are for now only vertical or horizontal
        self.line = line
        self.startx = line.split()[0].split(',')[0]
        self.starty = line.split()[0].split(',')[1]
        self.endx = line.split()[2].split(',')[0]
        self.endy = line.split()[2].split(',')[1]
        self.direction = self.direction()
        self.points = self.points()

    def direction(self):
        if self.startx == self.endx:
            return 'vertical'
        elif self.starty == self.endy:
            return 'horizontal'
        else:
            return 'diagonal'

    def points(self):
        points = []
        if self.direction == 'vertical':
            if int(self.starty) < (int(self.endy) + 1):
                for y in range(int(self.starty), (int(self.endy) + 1)):
                    point = self.startx + ',' + str(y)
                    points.append(point)
            else:
                for y in range(int(self.endy), (int(self.starty) + 1)):
                    point = self.startx + ',' + str(y)
                    points.append(point)
        elif self.direction == 'horizontal':
            if int(self.startx) < (int(self.endx) + 1):
                for x in range(int(self.startx), (int(self.endx) + 1)):
                    point = str(x) + ',' + self.starty
                    points.append(point)
            else:
                for x in range(int(self.endx), (int(self.startx) + 1)):
                    point = str(x) + ',' + self.starty
                    points.append(point)
        elif self.direction == 'diagonal':
            x = int(self.startx)
            y = int(self.starty)
            if int(self.startx) < (int(self.endx) + 1):
                if int(self.starty) < (int(self.endy) +1):
                    for i in range(int(self.startx), (int(self.endx) + 1)):
                        point = str(x) + ',' + str(y)
                        points.append(point)
                        x += 1
                        y += 1                        
                else:
                    for i in range(int(self.startx), (int(self.endx) + 1)):
                        point = str(x) + ',' + str(y)
                        points.append(point)
                        x += 1
                        y -= 1
            else:
                if int(self.starty) < (int(self.endy) +1):
                    for i in range(int(self.endx), (int(self.startx) + 1)):
                        point = str(x) + ',' + str(y)
                        points.append(point)
                        x -= 1
                        y += 1
                else:
                    for i in range(int(self.endx), (int(self.startx) + 1)):
                        point = str(x) + ',' + str(y)
                        points.append(point)
                        x -= 1
                        y -= 1
        return points


class Fish:
    def __init__(self, my_dict) -> None:
        self.school = my_dict
        # school is a list of integers

    def next_day(self):
        zeros = self.school[0]
        for i in range(1,9):
            self.school[i-1] = self.school[i]
        self.school[6] += zeros
        self.school[8] = zeros

    def amount_fish(self):
        total = 0
        for i in range(9):
            total += self.school[i]
        return total

from aoc import read_input

# inputfile = 'input7-test.txt'
inputfile = 'input7.txt'
my_input = [int(x) for x in read_input(inputfile)[0].split(',')]

def fuel_cost(i):
    fuel = 0
    for x in range(1, i+1):
        fuel += x
    return fuel


sums = []
for i in range(min(my_input), max(my_input)):
    sum = 0
    for j in range(len(my_input)):
        # print(i, my_input[j])
        sum = sum + fuel_cost(abs(i - my_input[j]))
        # print(sum)
    sums.append(sum)

print('Minimum value is: ', min(sums))
# print(sums)
from aoc import read_input, Bingoboard

inputfile = 'input4.txt'
my_input = read_input(inputfile)

# first item in the list is the drawn numbers for the bingo
# We need to split that item so we get a list of drawn numbers (str)
draw = my_input.pop(0).split(',')
# each board is an empty line in the file so now it's an empty string in the list
# plus five lines of rows with five numbers. below I split the complete list with all 
# boards into chuncks of one board and store that in inputs
inputs = [my_input[x:x+6] for x in range(0, len(my_input), 6)]

boards = []
for board in inputs:
    board.remove('')
    boards.append(Bingoboard(board))

def part1(boards, draw):
    for d in draw:
        for b in boards:
            b.draw(d)
            if b.is_winner():
                return b.remaining_sum() * int(d)

print(part1(boards, draw))


def part2(boards, draw):
    for d in draw:
        print(d)
        for b in boards:
            b.draw(d)
        for b in boards:
            if b.is_winner():
                if len(boards) > 1:
                    boards.remove(b)
                else:
                    return b.remaining_sum() * int(d)

print(part2(boards, draw))
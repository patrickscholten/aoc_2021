from aoc import Vent, read_input
from collections import Counter

# inputfile = 'input5-test.txt'
inputfile = 'input5.txt'
my_input = read_input(inputfile)

def dups(my_input):
    all_points = []
    for i in my_input:
        v = Vent(i)
        for p in v.points:
            all_points.append(p)
    c = Counter(all_points)
    counter = 0
    for point, amount in c.items():
        if amount > 1:
            counter += 1
    return counter


number_duplicates = dups(my_input)
print(number_duplicates)
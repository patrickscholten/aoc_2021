from aoc import read_input, Report

my_input = read_input('input3.txt')
r = Report(my_input)
print('power consumption: ', r.power_consumption())
print('life support rating: ', r.life_support_rating())